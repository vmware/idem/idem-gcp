SLS properties documentation
============================

The Idem GCP plugin doesn't attempt to invent yet another structure of API resources and properties which the user needs to learn.

It uses the same services, resources and fields as exposed in the GCP REST API and therefore any GCP documentation, examples and forum discussions on the web could be useful for creating and troubleshooting your SLS files.

Refer to the `GCP API explorer <https://developers.google.com/apis-explorer>`_ for a list of Google's exposed APIs and find the one for your use case.
For example, if you want to get more information on managing a GCP instance, click on `Compute Engine API v1`, then expand the `instances` dropdown and click on `Overview` or the specific method you need.

Since some services have multiple API versions, this is the current list of supported services and their API versions:
 * Compute Engine - v1
 * Cloud Key Management Service (KMS) - v1
 * Identity and Access Management (IAM) - v1
 * Cloud Storage - v1

The GCP plugin currently supports a subset of the exposed GCP services, along with their resources, properties and methods.

Refer to the :doc:`exec modules </ref/exec/index>` and :doc:`state modules </ref/states/index>` documentation to find the currently
supported properties and methods, along with documentation and examples.

The only transformation the GCP plugin performs upon the native GCP properties is converting the field names to snake case
in order to follow common `idem` conventions. So for example the "networkInterfaces" field of GCP instances becomes "network_interfaces" in idem.

Also as per convention resource names are in singular form, which differs from the GCP client library convention.
This results in "instances" resource of GCP to have the "instance" name in idem.

Additionally you may find that certain field names, which coincide with python built-in names, have underscore (_) appended to them.
For example, the following signature of the method listing crypto keys:

``async idem_gcp.exec.gcp.cloudkms.crypto_key.list_(hub, ctx, key_ring: str, filter_: (<class 'str'>, 'alias=filter') = None, order_by: str = None) → Dict[str, Any]``

may look odd, because both "list" and "filter" are reserved python keywords.

However, this doesn't affect the invocation as you could call the method using the "list" and "filter" names. This is exactly what the 'alias=filter' means.

Note: These implicit transformations are currently handled on input, but not yet upon output. So you might notice that when getting, listing and describing resources with such clashing properties,
the output contains the escaped names instead of the original GCP ones. These are also valid resource representations and work well as SLS input to ``idem state``
