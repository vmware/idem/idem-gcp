Install
=======

Idem is supported on Linux, macOS and Windows.

Packages and installers for the above operating systems are being worked on, but in the meantime, you can install Idem with Poetry. See `Installing Idem <https://docs.idemproject.io/getting-started/en/latest/topics/gettingstarted/installing.html>`_ to learn how to install Poetry.

After you install Poetry, you can install ``idem-gcp``.

First, ensure that you are in the same directory as your ``pyproject.toml`` file. Optionally, you can specify the directory containing your ``pyproject.toml`` file by using the ``--directory=DIRECTORY (-C)`` option.

Then run the following command to install ``idem-gcp``:

.. code-block:: bash

  poetry add idem-gcp

Configure idem-gcp
++++++++++++++++++

Next, click here to :doc:`configure idem-gcp </quickstart/configure/index>`
