"""Metadata module for managing Accelerator Types."""

PATH = "projects/{project}/zones/{zone}/acceleratorTypes/{acceleratorType}"

NATIVE_RESOURCE_TYPE = "compute.accelerator_types"
