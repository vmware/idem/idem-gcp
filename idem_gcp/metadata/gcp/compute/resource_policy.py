"""Metadata module for managing Resource Policies."""

PATH = "projects/{project}/regions/{region}/resourcePolicies/{resourcePolicy}"

NATIVE_RESOURCE_TYPE = "compute.resource_policies"
