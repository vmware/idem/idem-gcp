State
=====

The *State* command allows you to ensure your environment is configured according to your specification.
You specify your configuration in an *SLS* file, also known as a *state* file.

.. include:: ./_includes/auth-note.rst

Describe Example
++++++++++++++++

First let's describe our disks.

.. code-block:: bash

    idem describe gcp.compute.disk

Output:

.. code-block:: sls

    projects/<project_name>/zones/us-central1-a/disks/<disk_name>:
      gcp.compute.disk.present:
      - name: <disk_name>
      - size_gb: '10'
      - zone: us-central1-a
      - type: https://www.googleapis.com/compute/v1/projects/<project_name>/zones/us-central1-a/diskTypes/pd-balanced
      - label_fingerprint: 42WmSpB8rSM=
      - physical_block_size_bytes: '4096'
      - resource_policies:
        - https://www.googleapis.com/compute/v1/projects/<project_name>/regions/us-central1/resourcePolicies/<policy_name>
      - resource_id: projects/<project_name>/zones/us-central1-a/disks/<disk_name>

Use Idem State
______________

You'll notice that `size_gb` is set to `10`. If we want to
change that value to `20` we can take the above output from the Describe
example and create a file with `.sls` extension and use that to update the
running configuration.

Copy the above text output to ~/my_disk.sls, change `size_gb` to
`20` and then run the following command:

.. code-block:: bash

    idem state ~/my_disk.sls

Output:

.. code-block:: yaml

          ID: projects/<project_name>/zones/us-central1-a/disks/<disk_name>
    Function: gcp.compute.disk.present
      Result: True
     Comment: ("Updated gcp.compute.disk '<disk_name>'",)
     Changes:
    old:
        ----------
        size_gb:
            10
    new:
        ----------
        size_gb:
            20

Here you'll notice that the `old` field shows us the previous value for
`size_gb` and the `new` field shows us the new value.

Now that you know how to run basic idem commands, learn the more advanced :doc:`Usage specifics.  </quickstart/specifics/index>`
