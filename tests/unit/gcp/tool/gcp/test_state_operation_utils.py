from idem_gcp.tool.gcp.state_operation_utils import StateOperations


def test_changed_properties_dict_empty_resource_body_no_changes(hub):
    patch_operations_dict = {
        "some_prop_1": (
            "hub_ref_1",
            ("hub_ref_param_1", "hub_ref_param_2"),
            False,
            True,
        ),
        "some_prop_2": (
            "hub_ref_2",
            ("hub_ref_param",),
            False,
            True,
        ),
    }

    resource_type = "compute.instance"
    result = {"old_state": {"some_prop_1": 5, "some_prop_3": 2}}
    resource_body = {}
    state_operations = StateOperations(
        hub, resource_type, patch_operations_dict, result, resource_body
    )
    assert state_operations.old_properties_dict == {"some_prop_1": 5}
    assert state_operations.changed_properties_dict == {
        "some_prop_1": {},
        "some_prop_2": {},
    }


def test_changed_properties_dict_resource_body_falsey_value_changes(hub):
    patch_operations_dict = {
        "some_prop": (
            "hub_ref",
            ("hub_ref_param_1", "hub_ref_param_2"),
            False,
            True,
        )
    }

    resource_type = "compute.instance"
    result = {
        "old_state": {
            "some_prop": True,
        }
    }
    resource_body = {"some_prop": False}
    state_operations = StateOperations(
        hub, resource_type, patch_operations_dict, result, resource_body
    )
    assert state_operations.old_properties_dict == {"some_prop": True}
    assert state_operations.changed_properties_dict.get("some_prop") != {}


def test_changed_properties_dict_exclude_paths_respected(hub):
    patch_operations_dict = {
        "network_interfaces": (
            hub.tool.gcp.compute.instance.update_network_interfaces,
            ("param_1", "param_2"),
            True,
        )
    }

    resource_type = "compute.instance"
    result = {
        "old_state": {
            "network_interfaces": [{"network_ip": "10.10.10.10"}],
        }
    }
    resource_body = {"network_interfaces": [{"network_ip": "20.20.20.20"}]}
    state_operations = StateOperations(
        hub, resource_type, patch_operations_dict, result, resource_body
    )
    assert state_operations.changed_properties_dict.get("network_interfaces") == {}
