import time
from collections import ChainMap

import pytest

from tests.utils import generate_unique_name

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": generate_unique_name("idem-test-firewall"),
}

PRESENT_CREATE_STATE = {
    "name": PARAMETER["name"],
    "description": "cicd tests firewall instance",
    "allowed": [
        {"ip_protocol": "tcp", "ports": ["2222", "2223"]},
    ],
    "direction": "EGRESS",
    "disabled": True,
}

RESOURCE_TYPE_FIREWALL = "compute.firewall"


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_create")
def test_firewall_present_create(hub, gcp_project, idem_cli, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_FIREWALL, PRESENT_CREATE_STATE, __test
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_FIREWALL}", name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_FIREWALL}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_update", depends=["present_create"])
def test_firewall_present_update(hub, gcp_project, idem_cli, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_FIREWALL,
        {"name": PARAMETER["name"], "disabled": False},
        __test,
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_FIREWALL}", name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_FIREWALL}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )
    changes_new = hub.tool.gcp.utils.compare_states(
        PRESENT_CREATE_STATE, ret["new_state"], "compute.firewall"
    )
    changes_old = hub.tool.gcp.utils.compare_states(
        PRESENT_CREATE_STATE, ret["old_state"], "compute.firewall"
    )
    assert (
        changes_new["values_changed"]["root['disabled']"]["new_value"] == False
        and changes_new["values_changed"]["root['disabled']"]["old_value"] == True
        and not changes_old.get("values_changed")
    )


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["present_update"])
async def test_firewall_describe(hub, ctx, gcp_project):
    resource_id = hub.tool.gcp.resource_prop_utils.construct_resource_id(
        "compute.firewall", {"project": gcp_project, "firewall": PARAMETER["name"]}
    )
    describe_ret = await hub.states.gcp.compute.firewall.describe(ctx)
    assert resource_id in describe_ret
    assert f"gcp.{RESOURCE_TYPE_FIREWALL}.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get(
        f"gcp.{RESOURCE_TYPE_FIREWALL}.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))

    assert PARAMETER["name"] == described_resource_map["name"]
    assert resource_id == described_resource_map["resource_id"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="absent",
    depends=["describe"],
)
def test_instance_absent(hub, idem_cli, tests_dir, __test, gcp_project):
    global PARAMETER
    resource_id = hub.tool.gcp.resource_prop_utils.construct_resource_id(
        "compute.firewall", {"project": gcp_project, "firewall": PARAMETER["name"]}
    )
    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_FIREWALL,
        PARAMETER["name"],
        resource_id,
        test=__test,
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_FIREWALL}", name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_FIREWALL}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )

    assert not ret.get("new_state")


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_create_get_resource_only_with_resource_id", depends=["absent"]
)
def test_firewall_present_create_get_resource_only_with_resource_id(
    hub, gcp_project, idem_cli, tests_dir, __test
):
    global PARAMETER
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_FIREWALL,
        PRESENT_CREATE_STATE,
        __test,
        ["--get-resource-only-with-resource-id"],
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_FIREWALL}", name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_FIREWALL}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_update_get_resource_only_with_resource_id",
    depends=["present_create_get_resource_only_with_resource_id"],
)
def test_firewall_present_update_flagged(hub, gcp_project, idem_cli, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_FIREWALL,
        {"name": PARAMETER["name"], "disabled": False},
        __test,
        ["--get-resource-only-with-resource-id"],
    )

    if __test:
        assert ret["result"], ret["comment"]
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_FIREWALL}", name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert not ret["result"], ret["comment"]
        assert (
            hub.tool.gcp.comment_utils.already_exists_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_FIREWALL}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.fixture(autouse=True, scope="module")
async def cleanup_firewalls(hub, ctx, gcp_project):
    try:
        yield
    finally:
        get_ret = await hub.exec.gcp.compute.firewall.get(
            ctx, project=gcp_project, name=PARAMETER["name"]
        )
        if not get_ret["result"]:
            hub.log.error(get_ret)
        elif not get_ret["ret"]:
            hub.log.info(f"Already deleted or not created {get_ret}")
        else:
            call_delete = True
            delete_attempts = 0
            while call_delete:
                delete_attempts += 1
                delete_ret = await hub.exec.gcp_api.client.compute.firewall.delete(
                    ctx, project=gcp_project, firewall=PARAMETER["name"]
                )
                if not delete_ret["result"]:
                    if (
                        all(
                            [
                                "resourceNotReady" not in comment
                                for comment in delete_ret["comment"]
                            ]
                        )
                        or delete_attempts > 2
                    ):
                        hub.log.error(delete_ret)
                        return
                    time.sleep(3)
