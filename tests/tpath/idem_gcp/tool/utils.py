from typing import Dict
from typing import List

import yaml
from dict_tools.data import NamespaceDict
from pytest_idem import runner


def call_present_from_properties(
    hub,
    idem_cli,
    resource_type: str,
    present_state_properties: Dict,
    test: bool = False,
    additional_kwargs: List[str] = None,
) -> Dict:
    name = present_state_properties["name"]

    if isinstance(present_state_properties, NamespaceDict):
        # yaml serializer does not handle NamespaceDict class properly, convert it to regular dictionary
        present_state_properties = hub.tool.gcp.utils.convert_to_regular_dict(
            present_state_properties
        )

    present_state_str = yaml.safe_dump(
        {
            name: {
                f"gcp.{resource_type}.present": [
                    {k: v} for k, v in present_state_properties.items()
                ]
            }
        }
    )

    present_state_ret = hub.tool.utils.run_idem_state(
        idem_cli, present_state_str, test, additional_kwargs
    )
    return hub.tool.gcp.utils.get_esm_tagged_data(
        present_state_ret, f"gcp.{resource_type}"
    )


def call_present_from_sls(
    hub, idem_cli, present_state_sls_str: str, test: bool = False, *additional_kwargs
) -> Dict:
    present_state_dict = yaml.safe_load(present_state_sls_str)
    name = list(present_state_dict.keys())[0]
    gcp_resource_type = list(present_state_dict[name].keys())[0].replace(".present", "")

    present_state_ret = hub.tool.utils.run_idem_state(
        idem_cli, present_state_sls_str, test, additional_kwargs
    )
    return hub.tool.gcp.utils.get_esm_tagged_data(
        present_state_ret, f"{gcp_resource_type}"
    )


def call_exec_from_sls(
    hub,
    idem_cli,
    sls_str: str,
) -> Dict:
    exec_state_ret = hub.tool.utils.run_idem_state(idem_cli, sls_str)
    return hub.tool.gcp.utils.get_esm_tagged_data(exec_state_ret, "exec")


def call_present(
    hub,
    idem_cli,
    resource_type: str,
    present_state_properties: Dict,
    test: bool = False,
) -> Dict:
    name = present_state_properties["name"]
    present_format = {
        name: {
            f"gcp.{resource_type}.present": [
                {k: v} for k, v in present_state_properties.items()
            ]
        }
    }
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(yaml.dump(present_format))
        args = ["--acct-profile=test_development_idem_gcp"]
        if test:
            args.append("--test")
        state_ret = idem_cli(
            "state",
            fh,
            *args,
            check=True,
        ).json

        return hub.tool.gcp.utils.get_esm_tagged_data(state_ret, f"gcp.{resource_type}")


def call_absent(
    hub,
    idem_cli,
    resource_type: str,
    name: str,
    resource_id: str,
    zone: str = None,
    project: str = None,
    test: bool = False,
    absent_params: Dict[str, str] = None,
    additional_kwargs: List[str] = None,
) -> Dict:
    params_string = ""
    if absent_params:
        for absent_param, param_value in absent_params.items():
            params_string += " " * 14
            param_string = f"- {absent_param}: {param_value}"
            params_string += f"{param_string}\n"

    if resource_id:
        absent_state_str = f"""
            {name}:
              gcp.{resource_type}.absent:
              - resource_id: {resource_id}"""
    elif name and zone and project:
        absent_state_str = f"""
            {name}:
              gcp.{resource_type}.absent:
              - name: {name}
              - zone: {zone}
              - project: {project}"""
    else:
        absent_state_str = f"""
            {name}:
              gcp.{resource_type}.absent:
              - name: {name}"""

    if params_string:
        absent_state_str = f"{absent_state_str}\n{params_string}"

    absent_state_ret = hub.tool.utils.run_idem_state(
        idem_cli, absent_state_str, test, additional_kwargs
    )
    return hub.tool.gcp.utils.get_esm_tagged_data(
        absent_state_ret, f"gcp.{resource_type}"
    )


def run_idem_state(
    hub, idem_cli, state: str, test: bool = False, additional_args: List = []
):
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(state)
        args = ["--acct-profile=test_development_idem_gcp"]
        if additional_args:
            for item in additional_args:
                args.append(item)
        if test:
            args.append("--test")
        return idem_cli(
            "state",
            fh,
            *args,
            check=True,
        ).json
