from collections import ChainMap
from typing import Any
from typing import Dict

import pytest
from pytest_idem import runner

from tests.utils import generate_unique_name

SIZE_GB = "1"

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": generate_unique_name("idem-test-disk"),
    "name2": generate_unique_name("idem-test-disk"),
    "size_gb": f"'{SIZE_GB}'",
    "resource_policies": [],
    "labels": {},
    "label_fingerprint": "",
    "snapshot_name": generate_unique_name("idem-test-snapshot"),
    "zone": "us-central1-a",
    "disk-name-from-snapshot": generate_unique_name("idem-test-disk"),
}

ABSENT_STATE = f"""
{PARAMETER["name"]}:
  gcp.compute.disk.absent:
  - resource_id: projects/tango-gcp/zones/us-central1-a/disks/{PARAMETER["name"]}
  - project: tango-gcp
  - zone: us-central1-a
"""

REGIONAL_DISK_STATE = """
{name}:
  gcp.compute.disk.present:
  - name: {name}
  - region: us-central1
  - project: tango-gcp
  - type_: projects/tango-gcp/zones/us-central1-a/diskTypes/pd-balanced
  - size_gb: 1
  - replica_zones:
    - projects/tango-gcp/zones/us-central1-a
    - projects/tango-gcp/zones/us-central1-f
"""

ZONAL_DISK_STATE = """
{name}:
  gcp.compute.disk.present:
  - name: {name}
  - zone: us-central1-a
  - project: tango-gcp
  - type_: projects/tango-gcp/zones/us-central1-a/diskTypes/pd-balanced
  - size_gb: 1
"""

UPDATE_STATE_DISK = {
    "name": PARAMETER["name"],
    "zone": "us-central1-a",
    "project": "tango-gcp",
    "description": "new updated description",
}

RESOURCE_TYPE_SNAPSHOT = "compute.snapshot"
RESOURCE_TYPE_DISK = "compute.disk"
FULL_RESOURCE_TYPE_DISK = f"gcp.{RESOURCE_TYPE_DISK}"


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
def test_disk_present(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    path_to = f"{tests_dir}/sls/compute/default_disk_present.sls"
    with open(path_to) as template:
        data_template = template.read()
        with runner.named_tempfile(suffix=".sls") as fh:
            fh.write_text(data_template.format(**PARAMETER))
            if __test:
                state_ret = idem_cli(
                    "state",
                    fh,
                    "--test",
                    "--acct-profile=test_development_idem_gcp",
                    check=True,
                ).json
            else:
                state_ret = idem_cli(
                    "state",
                    fh,
                    "--acct-profile=test_development_idem_gcp",
                    check=True,
                ).json
            ret = hub.tool.gcp.utils.get_esm_tagged_data(state_ret, "gcp.compute.disk")
            assert ret["result"], ret["comment"]
            new_resource = ret["new_state"]
        if __test:
            assert [
                hub.tool.gcp.comment_utils.would_create_comment(
                    resource_type=FULL_RESOURCE_TYPE_DISK, name=PARAMETER["name"]
                )
            ] == ret["comment"]
        else:
            PARAMETER["resource_id"] = new_resource.get("resource_id", "")
            PARAMETER["label_fingerprint"] = new_resource.get("label_fingerprint", "")
            # TODO: Here we assert
            assert (
                hub.tool.gcp.comment_utils.create_comment(
                    resource_type=FULL_RESOURCE_TYPE_DISK, name=PARAMETER["name"]
                )
                in ret["comment"]
            )
            assert new_resource["id_"]
        assert PARAMETER["name"] == new_resource.get("name")


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="create_disk_from_snapshot_present", depends=["present"])
def test_create_disk_from_snapshot_present(hub, ctx, idem_cli, tests_dir, __test):
    global PARAMETER
    if not __test:
        project = hub.tool.gcp.utils.get_project_from_account(ctx, None)
        snapshot_resource_id = (
            f"projects/{project}/global/snapshots/{PARAMETER['snapshot_name']}"
        )
        f"projects/{project}/zone/{PARAMETER['zone']}/{PARAMETER['disk-name-from-snapshot']}"

        create_snapshot_state = {
            "name": PARAMETER["snapshot_name"],
            "source_disk": f'projects/{project}/zones/{PARAMETER["zone"]}/disks/{PARAMETER["name"]}',
            "storage_locations": ["us-central1"],
            "description": "test description",
        }
        ret = hub.tool.utils.call_present_from_properties(
            idem_cli, RESOURCE_TYPE_SNAPSHOT, create_snapshot_state, __test
        )
        assert ret["result"], ret["comment"]
        assert not ret["old_state"]
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_SNAPSHOT}",
                name=PARAMETER["snapshot_name"],
            )
            in ret["comment"]
        )

        create_disk_from_snapshot_state = {
            "name": PARAMETER["disk-name-from-snapshot"],
            "source_snapshot": f'projects/{project}/global/snapshots/{PARAMETER["snapshot_name"]}',
            "zone": PARAMETER["zone"],
            "project": project,
        }

        ret = hub.tool.utils.call_present_from_properties(
            idem_cli, RESOURCE_TYPE_DISK, create_disk_from_snapshot_state, __test
        )
        new_disk = ret["new_state"]
        assert ret["result"], ret["comment"]
        assert not ret["old_state"]
        assert new_disk
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type=FULL_RESOURCE_TYPE_DISK,
                name=PARAMETER["disk-name-from-snapshot"],
            )
            in ret["comment"]
        )

        ret = hub.tool.utils.call_absent(
            idem_cli,
            RESOURCE_TYPE_SNAPSHOT,
            PARAMETER["snapshot_name"],
            snapshot_resource_id,
            test=__test,
        )
        assert (
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_SNAPSHOT}",
                name=PARAMETER["snapshot_name"],
            )
            in ret["comment"]
        )
        ret = hub.tool.utils.call_absent(
            idem_cli,
            RESOURCE_TYPE_DISK,
            new_disk["name"],
            None,
            new_disk["zone"],
            project,
            __test,
        )

        assert ret["result"], ret["comment"]
        assert (
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type=FULL_RESOURCE_TYPE_DISK, name=new_disk["name"]
            )
            in ret["comment"]
        )


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="empty_update", depends=["present"])
def test_disk_empty_update(hub, idem_cli, tests_dir, __test):
    global PARAMETER

    path_to = f"{tests_dir}/sls/compute/default_disk_present.sls"
    with open(path_to) as template:
        data_template = template.read()
        disk_sls = data_template.format(**PARAMETER)

    ret = hub.tool.utils.call_present_from_sls(idem_cli, disk_sls, __test)
    assert ret["result"], ret["comment"]
    assert ret["new_state"]
    assert ret["old_state"]
    assert ret["new_state"] == ret["old_state"]
    disk_returned = ret["new_state"]

    assert [
        hub.tool.gcp.comment_utils.up_to_date_comment(
            resource_type=FULL_RESOURCE_TYPE_DISK, name=PARAMETER["name"]
        )
    ] == ret["comment"]

    assert PARAMETER["resource_id"] == disk_returned.get("resource_id")
    assert PARAMETER["name"] == disk_returned.get("name")
    assert SIZE_GB == disk_returned.get("size_gb")
    assert not disk_returned.get("resource_policies")
    assert not disk_returned.get("labels")
    assert PARAMETER["label_fingerprint"] == disk_returned.get("label_fingerprint")


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="update", depends=["present"])
def test_disk_update(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    # TODO [DA]: Add value to resource_policies once 'default-schedule-1' can be found in the project
    #  also, add them back in the default_disk_present.sls file
    # PARAMETER["resource_policies"] = [
    #     "projects/tango-gcp/regions/us-central1/resourcePolicies/default-schedule-1"
    # ]
    PARAMETER["resource_policies"] = []
    PARAMETER["labels"] = {"label_key_test": "label_value_test"}
    PARAMETER["size_gb"] = "2"
    path_to = f"{tests_dir}/sls/compute/default_disk_present.sls"
    with open(path_to) as template:
        data_template = template.read()
        with runner.named_tempfile(suffix=".sls") as fh:
            fh.write_text(data_template.format(**PARAMETER))
            if __test:
                state_ret = idem_cli(
                    "state",
                    fh,
                    "--test",
                    "--acct-profile=test_development_idem_gcp",
                    check=True,
                ).json
            else:
                state_ret = idem_cli(
                    "state",
                    fh,
                    "--acct-profile=test_development_idem_gcp",
                    check=True,
                ).json
            ret = hub.tool.gcp.utils.get_esm_tagged_data(state_ret, "gcp.compute.disk")
            assert ret["result"], ret["comment"]
            updated_resource = ret["new_state"]

        if __test:
            assert [
                hub.tool.gcp.comment_utils.would_update_comment(
                    resource_type=FULL_RESOURCE_TYPE_DISK, name=PARAMETER["name"]
                )
            ] == ret["comment"]
        else:
            # TODO: Here we assert
            assert (
                hub.tool.gcp.comment_utils.update_comment(
                    resource_type=FULL_RESOURCE_TYPE_DISK, name=PARAMETER["name"]
                )
                in ret["comment"]
            )

            updated_resource["resource_policies"] = [
                hub.tool.gcp.resource_prop_utils.parse_link_to_resource_id(
                    policy, "compute.resource_policy"
                )
                for policy in updated_resource.get("resource_policies")
            ]

            assert PARAMETER["labels"] == updated_resource.get("labels")
            assert PARAMETER["size_gb"] == updated_resource.get("size_gb")
            assert hub.tool.gcp.state_comparison_utils.are_lists_identical(
                PARAMETER["resource_policies"], updated_resource["resource_policies"]
            )

        assert PARAMETER["name"] == updated_resource.get("name")
        assert PARAMETER["resource_id"] == updated_resource.get("resource_id")


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="update_non", depends=["present"])
def test_disk_update_non_updatable_properties(hub, idem_cli, tests_dir, __test):
    global PARAMETER

    # Make a change to description, which is a non-updatable property
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_DISK, UPDATE_STATE_DISK, __test
    )

    assert not ret["result"], ret["comment"]
    updated_resource = ret["new_state"]

    assert [
        hub.tool.gcp.comment_utils.non_updatable_properties_comment(
            FULL_RESOURCE_TYPE_DISK,
            PARAMETER["name"],
            {"description"},
        )
    ] == ret["comment"]

    assert ret["new_state"] == ret["old_state"]
    assert PARAMETER["name"] == updated_resource.get("name")
    assert PARAMETER["resource_id"] == updated_resource.get("resource_id")

    # Asserting that in the new state, description did not change as requested
    assert ret["new_state"].get("description") != UPDATE_STATE_DISK["description"]
    assert ret["new_state"].get("description") is None


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["update"])
async def test_disk_describe(hub, ctx):
    describe_ret = await hub.states.gcp.compute.disk.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    assert "gcp.compute.disk.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get("gcp.compute.disk.present")
    described_resource_map = dict(ChainMap(*described_resource))

    assert PARAMETER["name"] == described_resource_map["name"]
    assert PARAMETER["resource_id"] == described_resource_map["resource_id"]


@pytest.mark.skip(
    reason="The resource policy used in the test could not be found due to GCP issues."
)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="update", depends=["present"])
def test_disk_remove_resource_policies(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    PARAMETER["added_resource_policies"] = [
        "projects/tango-gcp/regions/us-central1/resourcePolicies/default-schedule-1"
    ]
    PARAMETER["resource_policies"] = []
    path_to = f"{tests_dir}/sls/compute/default_disk_present.sls"
    with open(path_to) as template:
        data_template = template.read()
        with runner.named_tempfile(suffix=".sls") as fh:
            fh.write_text(data_template.format(**PARAMETER))
            if __test:
                state_ret = idem_cli(
                    "state",
                    fh,
                    "--test",
                    "--acct-profile=test_development_idem_gcp",
                    check=True,
                ).json
            else:
                state_ret = idem_cli(
                    "state",
                    fh,
                    "--acct-profile=test_development_idem_gcp",
                    check=True,
                ).json
            ret = hub.tool.gcp.utils.get_esm_tagged_data(state_ret, "gcp.compute.disk")

        assert ret["result"], ret["comment"]
        PARAMETER["added_resource_policies"][0] in ret["old_state"][
            "resource_policies"
        ][0]
        updated_resource = ret["new_state"]
        assert not updated_resource.get("resource_policies", None)
        assert PARAMETER["name"] == updated_resource.get("name")
        assert PARAMETER["resource_id"] == updated_resource.get("resource_id")

        if __test:
            assert [
                hub.tool.gcp.comment_utils.would_update_comment(
                    resource_type="gcp.compute.disk", name=PARAMETER["name"]
                )
            ] == ret["comment"]
        else:
            # TODO: Here we assert
            assert (
                hub.tool.gcp.comment_utils.update_comment(
                    resource_type="gcp.compute.disk", name=PARAMETER["name"]
                )
                in ret["comment"]
            )


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["describe"])
def test_disk_absent(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_absent(
        idem_cli,
        "compute.disk",
        PARAMETER["name"],
        PARAMETER["resource_id"],
        test=__test,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state")
    assert not ret.get("new_state")

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type=FULL_RESOURCE_TYPE_DISK, name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type=FULL_RESOURCE_TYPE_DISK, name=PARAMETER["name"]
            )
            in ret["comment"]
        )


@pytest.fixture(scope="module")
async def new_disk(hub, ctx, idem_cli) -> Dict[str, Any]:
    # Create disk
    disk_create_sls_str = f"""
            {PARAMETER["name2"]}:
              gcp.compute.disk.present:
              - zone: us-central1-a
              - type: projects/tango-gcp/zones/us-central1-a/diskTypes/pd-balanced
              - size_gb: 10
              - guest_os_features:
                - type: VIRTIO_SCSI_MULTIQUEUE
                - type: SEV_CAPABLE
                - type: UEFI_COMPATIBLE
                - type: GVNIC
        """

    disk_present_ret = hub.tool.utils.call_present_from_sls(
        idem_cli, disk_create_sls_str, False
    )
    disk = disk_present_ret["new_state"]

    assert (
        hub.tool.gcp.comment_utils.create_comment(
            resource_type=FULL_RESOURCE_TYPE_DISK, name=PARAMETER["name2"]
        )
        in disk_present_ret["comment"]
    )
    assert PARAMETER["name2"] == disk["name"]

    yield disk


@pytest.mark.parametrize(**PARAMETRIZE)
def test_disk_absent_by_name_zone_project(ctx, hub, idem_cli, __test, new_disk):
    global PARAMETER

    project = hub.tool.gcp.utils.get_project_from_account(ctx, None)

    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_DISK,
        new_disk["name"],
        None,
        new_disk["zone"],
        project,
        __test,
    )

    assert ret["result"], ret["comment"]

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type=FULL_RESOURCE_TYPE_DISK, name=new_disk["name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type=FULL_RESOURCE_TYPE_DISK, name=new_disk["name"]
            )
            in ret["comment"]
        )

    assert ret["old_state"]
    assert not ret["new_state"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
def test_disk_already_absent(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_absent(
        idem_cli,
        "compute.disk",
        PARAMETER["name"],
        PARAMETER["resource_id"],
        test=__test,
    )

    absent_comment = hub.tool.gcp.comment_utils.already_absent_comment(
        "gcp.compute.disk", PARAMETER["name"]
    )
    assert any(absent_comment in c for c in ret["comment"]), ret["comment"]

    assert ret["result"], ret["comment"]
    assert not ret.get("new_state")
    assert not ret.get("old_state")


async def test_zonal_and_regional_disks(hub, ctx, idem_cli, cleaner):
    def create_disk(name: str, sls: str):
        sls_str = sls.format(
            **{
                "name": name,
            }
        )
        ret = hub.tool.utils.call_present_from_sls(idem_cli, sls_str)
        assert ret
        assert ret["result"], ret["comment"]
        disk = ret["new_state"]
        assert disk
        cleaner(disk, "compute.disk")
        return disk

    created_disk1 = create_disk(
        name=generate_unique_name("idem-test-disk"), sls=ZONAL_DISK_STATE
    )
    created_disk2 = create_disk(
        name=generate_unique_name("idem-test-disk"), sls=REGIONAL_DISK_STATE
    )

    ret = await hub.exec.gcp.compute.disk.get(
        ctx, resource_id=f"{created_disk1.get('resource_id')}"
    )
    assert ret["ret"], ret["comment"]
    assert ret["result"], ret["comment"]
    disk1 = ret["ret"]
    assert disk1.get("name") == created_disk1.get("name")
    assert disk1.get("zone") == created_disk1.get("zone")
    assert disk1.get("zone")
    assert not disk1.get("region")

    ret = await hub.exec.gcp.compute.disk.get(
        ctx, resource_id=f"{created_disk2.get('resource_id')}"
    )
    assert ret["ret"], ret["comment"]
    assert ret["result"], ret["comment"]
    disk2 = ret["ret"]
    assert disk2.get("name") == created_disk2.get("name")
    assert disk2.get("region") == created_disk2.get("region")
    assert not disk2.get("zone")
    assert disk2.get("region")
