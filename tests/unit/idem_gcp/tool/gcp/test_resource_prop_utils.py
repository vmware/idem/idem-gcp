from unittest.mock import MagicMock

import pytest


def test_parse_link_to_resource_id_invalid_link(hub):
    self_link = "https://www.googleapis.com/compute/v1/projects/test-project/regions1/test-region/diskTypes/type-name"
    resource_type = "compute.disk_type"

    result = hub.tool.gcp.resource_prop_utils.parse_link_to_resource_id(
        self_link, resource_type
    )

    assert not result


def test_parse_link_to_resource_id_valid_link(hub):
    self_link = "https://www.googleapis.com/compute/v1/projects/test-project/zones/test-zone/diskTypes/type-name"
    resource_type = "compute.disk_type"
    result = hub.tool.gcp.resource_prop_utils.parse_link_to_resource_id(
        self_link, resource_type
    )

    assert result == "projects/test-project/zones/test-zone/diskTypes/type-name"


@pytest.mark.parametrize(
    "resource_id,matches",
    [
        (
            "https://www.googleapis.com/compute/v1/projects/test-project/zones/test-zone/diskTypes/type-name",
            True,
        ),
        (
            "https://www.googleapis.com/compute/v1/projects/test-project/zones/test-zone/diskTypes0/type-name",
            False,
        ),
        ("/projects/test-project/zones/test-zone/diskTypes/type-name", True),
        ("projects/test-project/zones/test-zone/diskTypes/type-name", True),
        ("projects/test-project/zones/test-zone/diskTypes/type-name0", True),
        ("test-project/zones/test-zone/diskTypes0/type-name", False),
        ("type-name", False),
    ],
)
def test_resource_type_matches(hub, resource_id, matches):
    assert matches == hub.tool.gcp.resource_prop_utils.resource_type_matches(
        resource_id, "compute.disk_type"
    )


def test_construct_resource_id_exact_props(hub):
    project = "test-project"
    zone = "test-zone"
    instance = "test-instance"
    props = {"project": project, "zone": zone, "instance": instance}
    resource_id_path = "projects/{project}/zones/{zone}/instances/{instance}"
    hub.tool.gcp.resource_prop_utils.get_resource_paths = MagicMock(
        return_value=[resource_id_path]
    )
    expected_resource_id = f"projects/{project}/zones/{zone}/instances/{instance}"
    resource_type = "compute.instance"
    resource_id = hub.tool.gcp.resource_prop_utils.construct_resource_id(
        resource_type, props
    )
    assert resource_id == expected_resource_id


def test_construct_resource_id_missing_props(hub):
    project = "test-project"
    zone = "test-zone"
    props = {
        "project": project,
        "zone": zone,
    }
    resource_id_path = "projects/{project}/zones/{zone}/instances/{instance}"
    hub.tool.gcp.resource_prop_utils.get_resource_paths = MagicMock(
        return_value=[resource_id_path]
    )
    resource_type = "compute.instance"
    resource_id = hub.tool.gcp.resource_prop_utils.construct_resource_id(
        resource_type, props
    )
    assert resource_id is None


def test_construct_resource_id_multiple_matches(hub):
    project_id = "test-project"
    email = "test@test.com"
    props = {
        "project_id": project_id,
        "email": email,
        "unique_id": "123495865403",
    }
    resource_id_paths = [
        "projects/{project_id}/serviceAccounts/{email}",
        "projects/{project_id}/serviceAccounts/{unique_id}",
    ]
    hub.tool.gcp.resource_prop_utils.get_resource_paths = MagicMock(
        return_value=resource_id_paths
    )
    expected_resource_id = f"projects/{project_id}/serviceAccounts/{email}"
    resource_type = "test.dummy.service_account"
    resource_id = hub.tool.gcp.resource_prop_utils.construct_resource_id(
        resource_type, props
    )
    assert resource_id == expected_resource_id


def test_construct_resource_id_redundant_props(hub):
    project = "test-project"
    zone = "test-zone"
    instance = "test-instance"
    props = {
        "project": project,
        "zone": zone,
        "instance": instance,
        "redundant": "redundant",
    }
    resource_id_paths = ["projects/{project}/zones/{zone}/instances/{instance}"]
    hub.tool.gcp.resource_prop_utils.get_resource_paths = MagicMock(
        return_value=resource_id_paths
    )
    expected_resource_id = f"projects/{project}/zones/{zone}/instances/{instance}"
    resource_type = "compute.instance"
    resource_id = hub.tool.gcp.resource_prop_utils.construct_resource_id(
        resource_type, props
    )
    assert resource_id == expected_resource_id


def test_construct_resource_id_no_props(hub):
    resource_id_path = "projects/{project}/zones/{zone}/instances/{instance}"
    hub.tool.gcp.resource_prop_utils.get_resource_paths = MagicMock(
        return_value=[resource_id_path]
    )
    resource_type = "compute.instance"

    resource_id = hub.tool.gcp.resource_prop_utils.construct_resource_id(
        resource_type, None
    )
    assert resource_id is None


def test_construct_resource_id_name_contains_id(hub):
    project = "test-project"
    zone = "test-zone"
    instance = "test-instance"
    resource_id_path = "projects/{project}/zones/{zone}/instances/{instance}"
    expected_resource_id = f"projects/{project}/zones/{zone}/instances/{instance}"
    props = {
        "name": expected_resource_id,
        "project": "dummy",
        "zone": "dummy",
        "instance": "dummy",
    }
    hub.tool.gcp.resource_prop_utils.get_resource_paths = MagicMock(
        return_value=[resource_id_path]
    )
    resource_type = "compute.instance"
    resource_id = hub.tool.gcp.resource_prop_utils.construct_resource_id(
        resource_type, props
    )
    assert resource_id == expected_resource_id


def test_construct_resource_id_simple_name(hub):
    project = "test-project"
    zone = "test-zone"
    instance = "test-instance"
    props = {
        "name": instance,
        "project": project,
        "zone": zone,
        "instance": instance,
    }
    resource_id_path = "projects/{project}/zones/{zone}/instances/{instance}"
    hub.tool.gcp.resource_prop_utils.get_resource_paths = MagicMock(
        return_value=[resource_id_path]
    )
    expected_resource_id = f"projects/{project}/zones/{zone}/instances/{instance}"
    resource_type = "compute.instance"
    resource_id = hub.tool.gcp.resource_prop_utils.construct_resource_id(
        resource_type, props
    )
    assert resource_id == expected_resource_id


def test_get_elements_from_resource_id(hub):
    resource_type = "compute.snapshot"
    resource_paths = ["projects/{project}/global/snapshots/{snapshot}"]
    hub.tool.gcp.resource_prop_utils.get_resource_paths = MagicMock(
        return_value=resource_paths
    )
    project = "test-project"
    snapshot = "test-snapshot"
    with pytest.raises(ValueError) as e_info:
        hub.tool.gcp.resource_prop_utils.get_elements_from_resource_id(
            "compute.snapshot", f"projects/{project}/global/snapshot/{snapshot}"
        )
    assert str(
        e_info.value
    ) == hub.tool.gcp.comment_utils.ill_formed_resource_id_comment(
        resource_type, f"projects/{project}/global/snapshot/{snapshot}", resource_paths
    )

    els = hub.tool.gcp.resource_prop_utils.get_elements_from_resource_id(
        "compute.snapshot", f"projects/{project}/global/snapshots/{snapshot}"
    )
    assert els is not None
    assert els["project"] == project
    assert els["snapshot"] == snapshot


def test_properties_mismatch_resource_id(hub):
    project = "test-project"
    zone = "test-zone"
    instance = "test-instance"
    resource_type = "compute.instance"
    state_properties = {
        "project": project,
        "zone": zone,
        "instance": instance,
    }
    resource_id = f"projects/{project}/zones/asia/instances/{instance}"
    hub.tool.gcp.resource_prop_utils.get_resource_paths = MagicMock(
        return_value=["projects/{project}/zones/{zone}/instances/{instance}"]
    )
    expected_mismatch = (
        hub.tool.gcp.resource_prop_utils.properties_mismatch_resource_id(
            resource_type, resource_id, state_properties
        )
    )

    assert expected_mismatch


def test_properties_no_mismatch_resource_id(hub):
    project = "test-project"
    zone = "test-zone"
    instance = "test-instance"
    resource_type = "compute.instance"
    state_properties = {
        "project": project,
        "zone": zone,
        "instance": instance,
    }
    resource_id = f"projects/{project}/zones/{zone}/instances/{instance}"
    expected_mismatch = (
        hub.tool.gcp.resource_prop_utils.properties_mismatch_resource_id(
            resource_type, resource_id, state_properties
        )
    )

    assert not expected_mismatch


def test_get_exclude_keys_from_transformation_multiple_nested(hub):
    name = "test-instance"
    resource_type = "compute.instance"
    state_properties = {
        "name": name,
        "labels": {"label_key": "label_value"},
        "disks": [
            {
                "boot": True,
                "initialize_params": {
                    "labels": {"nested_label_key_1": "nested_label_value_1"}
                },
            },
            {
                "boot": False,
                "initialize_params": {
                    "labels": {"nested_label_key_2": "nested_label_value_2"}
                },
            },
        ],
    }

    actual_keys_list = (
        hub.tool.gcp.resource_prop_utils.get_exclude_keys_from_transformation(
            state_properties, resource_type, is_raw_resource=False
        )
    )
    assert actual_keys_list

    expected_keys_list = ["label_key", "nested_label_key_1", "nested_label_key_2"]
    assert len(actual_keys_list) == len(expected_keys_list)

    for key in expected_keys_list:
        assert key in actual_keys_list


def test_get_exclude_keys_from_transformation_not_added_properties_to_be_excluded(hub):
    name = "test-instance"
    source_image = "test-source-image"
    resource_type = "compute.instance"
    state_properties = {
        "name": name,
        "disks": [
            {"boot": True, "initialize_params": {"source_image": source_image}},
            {"boot": False, "initialize_params": {"source_image": source_image}},
        ],
    }

    actual_keys_list = (
        hub.tool.gcp.resource_prop_utils.get_exclude_keys_from_transformation(
            state_properties, resource_type, is_raw_resource=False
        )
    )
    assert not actual_keys_list
    assert len(actual_keys_list) == 0


def test_get_exclude_keys_from_transformation_properties_to_be_excluded_set_to_none(
    hub,
):
    name = "test-instance"
    source_image = "test-source-image"
    resource_type = "compute.instance"
    state_properties = {
        "name": name,
        "labels:": None,
        "disks": [
            {
                "boot": True,
                "initialize_params": {
                    "source_image": source_image,
                    "labels:": None,
                },
            },
            {
                "boot": False,
                "initialize_params": {
                    "source_image": source_image,
                },
            },
        ],
    }

    actual_keys_list = (
        hub.tool.gcp.resource_prop_utils.get_exclude_keys_from_transformation(
            state_properties, resource_type, is_raw_resource=False
        )
    )
    assert not actual_keys_list
    assert len(actual_keys_list) == 0
