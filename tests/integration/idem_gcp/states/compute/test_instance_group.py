from collections import ChainMap

import pytest

from tests.utils import generate_unique_name

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": generate_unique_name("idem-test-instance-group"),
    "zone": "us-central1-a",
    "project": "tango-gcp",
    "description": "some instance group desc",
}

RESOURCE_TYPE = "compute.instance_group"
RESOURCE_ID = f"projects/{PARAMETER['project']}/zones/{PARAMETER['zone']}/instanceGroups/{PARAMETER['name']}"

INSTANCE_GROUP_SPEC_CREATE = """
{name}:
  gcp.compute.instance_group.present:
  - name: {name}
  - zone: {zone}
  - project: {project}
  - description: {description}"""

INSTANCE_GROUP_SPEC_UPDATE = """
{name}:
  gcp.compute.instance_group.present:
  - name: {name}
  - zone: {zone}
  - project: {project}
  - description: {description}
  - named_ports:
    - name: test
      port: 1234"""


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
def test_instance_group_create(hub, ctx, idem_cli, __test, cleaner):
    global PARAMETER

    sls_str = INSTANCE_GROUP_SPEC_CREATE.format(**PARAMETER)
    ret = hub.tool.utils.call_present_from_sls(idem_cli, sls_str, __test)
    assert ret["result"], ret["comment"]
    assert ret["new_state"]

    cleaner(ret["new_state"], "compute.instance_group")

    instance_group = ret["new_state"]
    PARAMETER["test_instance_group"] = instance_group

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type="gcp.compute.instance_group", name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type="gcp.compute.instance_group", name=PARAMETER["name"]
            )
            in ret["comment"]
        )

    assert instance_group["name"] == PARAMETER["name"]
    assert instance_group["description"] == PARAMETER["description"]
    assert (
        hub.tool.gcp.resource_prop_utils.parse_link_to_resource_id(
            instance_group["resource_id"], "compute.instance_group"
        )
        is not None
    )


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_up_to_date", depends=["present"])
def test_instance_group_up_to_date(hub, idem_cli, tests_dir, __test):
    sls_str = INSTANCE_GROUP_SPEC_CREATE.format(**PARAMETER)
    ret = hub.tool.utils.call_present_from_sls(idem_cli, sls_str, __test)
    assert ret["result"], ret["comment"]
    assert ret["new_state"]
    assert ret["new_state"].get("resource_id") == RESOURCE_ID
    assert [
        hub.tool.gcp.comment_utils.up_to_date_comment(
            resource_type=f"gcp.{RESOURCE_TYPE}", name=PARAMETER["name"]
        )
    ] == ret["comment"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_update", depends=["present_up_to_date"])
def test_instance_group_update(hub, idem_cli, tests_dir, __test):
    update_sls_str = INSTANCE_GROUP_SPEC_UPDATE.format(**PARAMETER)

    ret = hub.tool.utils.call_present_from_sls(idem_cli, update_sls_str, __test)

    assert ret["result"], ret["comment"]
    assert ret.get("new_state")
    assert ret["new_state"].get("resource_id") == RESOURCE_ID

    changes = hub.tool.gcp.utils.compare_states(
        ret["old_state"],
        ret["new_state"],
        "compute.instance_group",
    )

    assert ret["new_state"]["named_ports"][0]["name"] == "test"
    assert ret["new_state"]["named_ports"][0]["port"] == 1234

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE}", name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )
        assert {"root['named_ports']", "root['fingerprint']"} == changes[
            "relevant_changes"
        ]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present__with_named_ports_up_to_date", depends=["present_update"]
)
def test_instance_group_with_named_ports_up_to_date(hub, idem_cli, tests_dir, __test):
    update_sls_str = INSTANCE_GROUP_SPEC_UPDATE.format(**PARAMETER)
    ret = hub.tool.utils.call_present_from_sls(idem_cli, update_sls_str, __test)
    assert ret["result"], ret["comment"]
    assert ret["new_state"]
    assert ret["new_state"].get("resource_id") == RESOURCE_ID
    assert [
        hub.tool.gcp.comment_utils.up_to_date_comment(
            resource_type=f"gcp.{RESOURCE_TYPE}", name=PARAMETER["name"]
        )
    ] == ret["comment"]


@pytest.mark.asyncio
@pytest.mark.dependency(
    name="describe", depends=["present__with_named_ports_up_to_date"]
)
async def test_instance_group_describe(hub, ctx):
    ret = await hub.states.gcp.compute.instance_group.describe(ctx)
    for resource_id in ret:
        described_resource = ret[resource_id].get("gcp.compute.instance_group.present")
        assert described_resource
        instance_group = dict(ChainMap(*described_resource))
        assert instance_group.get("resource_id") == resource_id
        # todo uncomment once both zonal and regional resources are supported
        # assert hub.tool.gcp.resource_prop_utils.parse_link_to_resource_id(
        #     resource_id, "compute.instance_group"
        # )


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["describe"])
def test_instance_group_absent(hub, idem_cli, __test):
    ret = hub.tool.utils.call_absent(
        idem_cli, RESOURCE_TYPE, None, RESOURCE_ID, test=__test
    )

    if __test:
        assert ret
        assert ret.get("comment")
        assert [
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE}", name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        # Considers the resource deleted.
        assert ret
        assert ret.get("comment")
        assert [
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE}", name=PARAMETER["name"]
            )
        ] == ret["comment"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
def test_instance_group_already_absent(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE,
        PARAMETER["name"],
        RESOURCE_ID,
        test=__test,
    )

    absent_comment = hub.tool.gcp.comment_utils.already_absent_comment(
        "gcp.compute.instance_group", PARAMETER["name"]
    )
    assert any(absent_comment in c for c in ret["comment"])
    assert ret["result"], ret["comment"]
    assert not ret.get("new_state")
    assert not ret.get("old_state")
