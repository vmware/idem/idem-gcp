from collections import ChainMap

import pytest

from tests.utils import generate_unique_name

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": generate_unique_name("idem-test-target-pool"),
}
RESOURCE_TYPE_TARGET_POOL = "compute.target_pool"
PRESENT_STATE_TARGET_POOL = {
    "name": PARAMETER["name"],
}


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.asyncio
def test_present_no_op(hub, idem_cli, __test):
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_TARGET_POOL, PRESENT_STATE_TARGET_POOL, __test
    )
    assert ret
    assert ret["result"]
    assert (
        hub.tool.gcp.comment_utils.no_resource_create_update_comment(
            "gcp.compute.target_pool"
        )
        in ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_absent_no_op(hub, idem_cli, __test):
    global PARAMETER
    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_TARGET_POOL,
        PARAMETER["name"],
        resource_id=None,
        test=__test,
    )

    assert ret
    assert ret["result"]
    assert (
        hub.tool.gcp.comment_utils.no_resource_delete_comment("gcp.compute.target_pool")
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_describe(hub, ctx):
    ret = await hub.states.gcp.compute.target_pool.describe(ctx)
    if len(ret) > 0:
        for resource_id in ret:
            described_resource = ret[resource_id].get("gcp.compute.target_pool.present")
            assert described_resource
            target_pool = dict(ChainMap(*described_resource))
            assert target_pool.get("resource_id") == resource_id
