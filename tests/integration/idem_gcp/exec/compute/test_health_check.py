import pytest

from tests.utils import generate_unique_name

PARAMETER = {
    "health_check_1_name": generate_unique_name("idem-test-health-check"),
}

RESOURCE_TYPE_HEALTH_CHECK = "compute.health_check"

HEALTH_CHECK_SPEC = """
{health_check_name}:
  gcp.compute.health_check.present:
  - name: {health_check_name}
  - type_: TCP
  - tcp_health_check:
      port: 50000
"""


@pytest.fixture(scope="module")
def health_check_1(hub, idem_cli, cleaner):
    global PARAMETER

    # Create health check
    present_state_sls = HEALTH_CHECK_SPEC.format(
        **{
            "health_check_name": PARAMETER["health_check_1_name"],
        }
    )

    health_check_ret = hub.tool.utils.call_present_from_sls(
        idem_cli,
        present_state_sls,
    )

    assert health_check_ret["result"], health_check_ret["comment"]
    resource_id = health_check_ret["new_state"]["resource_id"]
    PARAMETER["health_check_1_resource_id"] = resource_id

    yield health_check_ret["new_state"]

    cleaner(health_check_ret["new_state"], "compute.health_check")


@pytest.mark.asyncio
async def test_get_missing_name_arg(hub, ctx):
    ret = await hub.exec.gcp.compute.health_check.get(ctx)
    assert not ret["result"], ret["comment"]
    assert not ret["ret"]
    assert ret["comment"]
    assert 'Missing required parameter "healthCheck"' in ret["comment"]


@pytest.mark.asyncio
async def test_get_by_name(hub, ctx, health_check_1):
    ret = await hub.exec.gcp.compute.health_check.get(
        ctx,
        name=health_check_1["name"],
    )
    assert ret["result"], ret["comment"]
    assert not ret["comment"]
    assert health_check_1["resource_id"] == ret["ret"].get("resource_id", None)
    assert health_check_1["name"] in ret["ret"].get("name", None)


@pytest.mark.asyncio
async def test_get_by_resource_id(hub, ctx, health_check_1):
    ret = await hub.exec.gcp.compute.health_check.get(
        ctx, resource_id=health_check_1["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"], ret["comment"]
    assert not ret["comment"]
    assert health_check_1["resource_id"] == ret["ret"].get("resource_id", None)
    assert health_check_1["name"] == ret["ret"].get("name", None)


@pytest.mark.asyncio
async def test_get_invalid_name(hub, ctx):
    invalid_name = "invalid-health-check-name"
    ret = await hub.exec.gcp.compute.health_check.get(
        ctx,
        name=invalid_name,
    )
    assert ret["result"], ret["comment"]
    assert not ret["ret"]
    assert any("result is empty" in c for c in ret["comment"])


@pytest.mark.asyncio
async def test_get_invalid_resource_id(hub, ctx):
    invalid_resource_id = (
        f"/projects/{ctx.acct.project_id}/global/healthChecks/some-name"
    )
    ret = await hub.exec.gcp.compute.health_check.get(
        ctx, resource_id=invalid_resource_id
    )
    assert ret["result"], ret["comment"]
    assert not ret["ret"]
    assert any("result is empty" in c for c in ret["comment"])


@pytest.mark.asyncio
async def test_list_invalid_project(hub, ctx):
    ret = await hub.exec.gcp.compute.health_check.list(ctx, project="invalid-project")
    assert not ret["result"], ret["comment"]
    assert not ret["ret"]
    assert ret["comment"]


@pytest.mark.asyncio
async def test_list_for_project(hub, ctx, health_check_1):
    ret = await hub.exec.gcp.compute.health_check.list(ctx)
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert not ret["comment"]
    assert len(ret["ret"]) > 0


@pytest.mark.asyncio
async def test_list_filter(hub, ctx, health_check_1):
    ret = await hub.exec.gcp.compute.health_check.list(
        ctx, filter_=f"name={health_check_1['name']}"
    )
    assert ret["result"], ret["comment"]
    assert not ret["comment"]
    assert ret["ret"]
    assert len(ret["ret"]) == 1
    assert health_check_1["name"] == ret["ret"][0].get("name", None)
    assert health_check_1["resource_id"] == ret["ret"][0].get("resource_id", None)


@pytest.mark.asyncio
async def test_list_invalid_filter(hub, ctx):
    ret = await hub.exec.gcp.compute.health_check.list(ctx, filter_="name eq unknown")
    assert ret["result"], ret["comment"]
    assert not ret["ret"]
    assert len(ret["ret"]) == 0
    assert not ret["comment"]
